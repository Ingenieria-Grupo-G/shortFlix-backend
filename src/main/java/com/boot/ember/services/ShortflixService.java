package com.boot.ember.services;

import java.io.Serializable;

import org.hibernate.Session;

import com.boot.ember.dao.ShortFilmDAO;
import com.boot.ember.dao.UserDAO;
import com.boot.ember.model.ShortFilm;
import com.boot.ember.model.User;
import com.boot.ember.service.runner.Runner;

/**
 * Servicio generico para guardar o recuperar
 * objetos de forma genreica. 
 */
public class ShortflixService {
	
	private ShortFilmDAO shortFilmDAO;
	private UserDAO userDAO;
	
	public ShortflixService(){
		this.shortFilmDAO = new ShortFilmDAO();
		this.userDAO = new UserDAO();
	}

	public void saveShortfilm(ShortFilm shortFilm) {
		 shortFilmDAO.save(shortFilm);
	}
	
	public ShortFilm getShortfilm(long id){
		return shortFilmDAO.get(id);
	}
	
	public void saveUser(User user){
		userDAO.save(user);
	}

	public User getUser(String mail){
		return userDAO.get(mail);
	}
}
