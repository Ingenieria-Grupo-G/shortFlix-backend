package com.boot.ember.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Director {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
    private long id;
	private String name;
	@ManyToMany(cascade = {CascadeType.ALL},mappedBy="directors")
	private Set<ShortFilm> directedShorts;
	
	public Director(String name){
		this.name = name;
		this.directedShorts = new HashSet<>();
	}
	
	public String getName(){
		return this.name;
	}
	
	public Set<ShortFilm> getDirectedShorts(){
		return this.directedShorts;
	}

	public void addDirectedShort(ShortFilm shortfilm) {
		this.directedShorts.add(shortfilm);
		
	}

}
