package com.boot.ember.model;

public enum Genre {
	ACTION,
	ANIMATION,
	COMEDY,
	DOCUMENTARY,
	DRAMA,
	ROMANCE,
	SCIFI,
	THRILLER;
	
}
