package com.boot.ember.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
public class ShortFilm {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
    private long id;
    private String title;
    private String cover;
    private String description;
    private double score;
    private String imdb;
    private Integer year;
    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name="ShortfilmActor", joinColumns={@JoinColumn(name="idShortFilm")}, 
    inverseJoinColumns={@JoinColumn(name="idActor")})
    private Set<Actor> actors;
    
    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name="ShortfilmWriter", joinColumns={@JoinColumn(name="idShortFilm")}, 
    inverseJoinColumns={@JoinColumn(name="idWriter")})
    private Set<Writer> writers;
    
    @ElementCollection(targetClass=Genre.class, fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    private Set<Genre> genres;
    
    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name="ShortfilmDirector", joinColumns={@JoinColumn(name="idShortFilm")}, 
    inverseJoinColumns={@JoinColumn(name="idDirector")})
    private Set<Director> directors;
    private String duration;
    private String video;

    public ShortFilm(){
    	}

    public ShortFilm(String title, String cover, String video, String description) {
        this.title = title;
        this.cover = cover;
        this.video = video;
        this.description = description;
    }
    
    public ShortFilm(String title, String cover, String description, double score,
    			 String imdb, Integer year, Set<Genre> genres, String duration, 
    			 Set<Writer> writers, Set<Actor> actors, Set<Director> directors, String video) {
        this.title = title;
        this.cover = cover;
        this.description = description;
        this.score = score;
        this.imdb = imdb;
        this.year = year;
        this.genres = genres;
        this.duration = duration;
        this.writers = writers;
        this.actors = actors;
        this.directors = directors;
        this.video = video;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

  public String getCover() {
    return cover;
  }

  public String getDescription() {
    return description;
  }

  public double getScore() {
    return score;
  }

  public String getImdb() {
    return imdb;
  }

  public Integer getYear() {
    return year;
  }

  public Set<Genre> getGenres() {
    return genres;
  }

  public String getDuration() {
    return duration;
  }

  public Set<Writer> getWriters() {
    return writers;
  }

  public Set<Actor> getActors() {
    return actors;
  }

  public Set<Director> getDirectors() {
    return directors;
  }

  public String getVideo() {
    return video;
  }
  
}
