package com.boot.ember.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Writer {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
    private long id;
	private String name;
	@ManyToMany(cascade = {CascadeType.ALL},mappedBy="actors")
	private Set<ShortFilm> writtenShorts;

	public Writer(String name) {
		this.name = name;
		this.writtenShorts = new HashSet<ShortFilm>();
	}

	public String getName() {
		return this.name;
	}

	public Set<ShortFilm> getWrittenShorts() {
		return this.writtenShorts;
	}

	public void addWrittenShorts(ShortFilm shortfilm) {
		this.writtenShorts.add(shortfilm);
		
	}

}
