package com.boot.ember.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Actor {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
    private long id;
	private String name;
	@ManyToMany(cascade = {CascadeType.ALL},mappedBy="actors")
	private Set<ShortFilm> starredShorts;

	public Actor(String name) {
		this.name = name;
		this.starredShorts = new HashSet<ShortFilm>();
	}

	public String getName() {
		return this.name;
	}

	public Set<ShortFilm> getStarredShorts() {
		return this.starredShorts;
	}

	public void addStarredShort(ShortFilm shortfilm) {
		this.starredShorts.add(shortfilm);
		
	}

}
