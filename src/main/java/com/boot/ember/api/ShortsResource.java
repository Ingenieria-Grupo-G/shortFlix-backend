package com.boot.ember.api;

import com.boot.ember.model.Director;
import com.boot.ember.model.ShortFilm;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
public class ShortsResource {

    List<ShortFilm> list = dummy_movies();

    @CrossOrigin
    @RequestMapping(value = "/api/shorts", method = RequestMethod.GET)
    public List<ShortFilm> getShorts() {
        return list;
    }

    @CrossOrigin
    @RequestMapping(value = "/api/short/{id}", method = RequestMethod.GET)
    public ShortFilm getShort(@PathVariable("id") long id) {
        return list.get((int) id);
    }

    @CrossOrigin
    @RequestMapping(value = "/api/new-short", method = RequestMethod.POST)
    public String newShort(HttpServletRequest request) {
        ShortFilm shortFilm = new ShortFilm(request.getParameter("title"),
                                            request.getParameter("cover"),
                                            request.getParameter("video"),
                                            request.getParameter("description"));
        list.add(shortFilm);
        return "success";
    }


    
    private List<ShortFilm> dummy_movies() {
      List<ShortFilm> list = new ArrayList<>();
      list.add(comeTogether());
      list.add(theGarden());
      list.add(olafsFrozen());
      list.add(zygote());
      list.add(deadpool());
      list.add(snowSteamIron());
      list.add(smilf());
      list.add(gong());
      list.add(touch());
      list.add(rakka());
      return list;
    }

    private ShortFilm comeTogether() {
      return new ShortFilm("Come Together: A Fashion Picture in Motion", 
    		  "https://images-na.ssl-images-amazon.com/images/M/MV5BZDc3M2VkZWMtM2NkNS00N2NmLTgxNzMtZGY5MGZiMjgyNjJlL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNTk5MDMzODc@._V1_UY268_CR116,0,182,268_AL_.jpg",
    		  "https://www.youtube.com/embed/VDinoNRC49c?modestbranding=1",
        "Lone passengers are traveling to join their loved ones for the Christmas holiday, but winter weather conspires to way-lay them.");
    }
    private ShortFilm theGarden() {
        return new ShortFilm("The Garden", "http://www.rocofilms.com/films/GRDN/posterMaxi.jpg",
          "https://www.youtube.com/embed/gI2Jy2DcKMQ?modestbranding=1",
          "In the Garden, humans get converted into A.I. constructs to survive. But Luc - a rebellious ballerina - would rather die than conform.");
      }
    private ShortFilm olafsFrozen() {
        return new ShortFilm("Olaf's Frozen Adventure", "https://images-na.ssl-images-amazon.com/images/M/MV5BMjQzNzA5NDEwOF5BMl5BanBnXkFtZTgwNTU0MjE2MjI@._V1_UX182_CR0,0,182,268_AL_.jpg",
        		"https://www.youtube.com/embed/hb8WDATVB6A?modestbranding=1", 
        		"A Christmas-themed special featuring characters from the Walt Disney Pictures film, 'Frozen'.");
      }
    private ShortFilm zygote() {
        return new ShortFilm("Zygote", "https://images-na.ssl-images-amazon.com/images/M/MV5BYzE2NDdlODYtMTc4My00OTljLTlmZTktNjM2NjA3YzliZTMyXkEyXkFqcGdeQXVyNjMxMDgyNzQ@._V1_UY268_CR3,0,182,268_AL_.jpg",
        "https://www.youtube.com/embed/pKWB-MVJ4sQ?modestbranding=1","Stranded in an Arctic mine, two lone survivors are forced to fight for their lives, evading and hiding from a new kind of terror."  );
      }
    private ShortFilm deadpool() {
        return new ShortFilm("Deadpool: No Good Deed", "https://images-na.ssl-images-amazon.com/images/M/MV5BODAxNDFhNGItMzdiMC00NDM1LWExYWUtZjNiMGIzYTc0MTM5XkEyXkFqcGdeQXVyMjYzMjA3NzI@._V1_UY268_CR3,0,182,268_AL_.jpg",
        		"https://www.youtube.com/embed/Z5ezsReZcxU?modestbranding=1"
        		,"Deadpool sees an opportunity to save the day, but it doesn't go entirely as planned.");
      }
    private ShortFilm snowSteamIron() {
        return new ShortFilm("Snow Steam Iron", "https://images-na.ssl-images-amazon.com/images/M/MV5BMTA0MGE3ZDAtNDJiNy00NDUzLWI5M2YtNjA"
        		+ "2NzhjNmMxNjgwXkEyXkFqcGdeQXVyMzQ3NTE0ODM@._V1_UY268_CR0,0,182,268_AL_.jpg",
        		"https://www.youtube.com/embed/Z5ezsReZcxU?modestbranding=1",
          "Snow gently falls on the blood-stained streets of a seedy out-of-time New York City. "
          );
      }
    private ShortFilm smilf() {
        return new ShortFilm("Smilf", "https://images-na.ssl-images-amazon.com/images/M/MV5BMTQ"
        		+ "2NjM0MDgzOF5BMl5BanBnXkFtZTgwNzQ3OTY4MzE@._V1_UY268_CR12,0,182,268_AL_.jpg",
        		"https://www.youtube.com/embed/Z5ezsReZcxU?modestbranding=1",
          "SMILF is about Bridgette Bird, a young single mother struggling to balance her old life of freedom with her new life as mom. "
          );
      }
    private ShortFilm gong() {
        return new ShortFilm("Gong shou dao", "https://images-na.ssl-images-amazon.com/images/M/MV5BMjNiM2M2NzEtYjhlZS00YjdiL"
        		+ "TlkMzktYmQ1MTZhZTc0MWRhXkEyXkFqcGdeQXVyMjExMzEyNTM@._V1_UX182_CR0,0,182,268_AL_.jpg",
        		"https://www.youtube.com/embed/Z5ezsReZcxU?modestbranding=1",
          "Master Ma is walking down the street, suddenly he sees the some words hiding between the green grass."
          );
      }
    private ShortFilm touch() {
        return new ShortFilm("Touch of evil", "https://images-na.ssl-images-amazon.com/images/M"
        		+ "/MV5BNWNjNDQxNjMtMDQyYy00Y2I1LWFiYjMtYmFkNmFlYTE0OWQ3XkEyXkFqcGdeQXVyMjA5MTIzMjQ@._V1_UX182_CR0,0,182,268_AL_.jpg",
        		"https://www.youtube.com/embed/Z5ezsReZcxU?modestbranding=1",
          "Some of 2011's stand-out film actors appear in a video gallery of cinematic villainy for New York Times Magazine."
          );
      }
    private ShortFilm rakka() {
        return new ShortFilm("Rakka", "https://images-na.ssl-images-amazon.com/images/M/MV5BZTRlZjRhMDItMjhlMi0"
        		+ "0MDNmLTg2OTgtZjliNWFhMDEwM2VhXkEyXkFqcGdeQXVyMjExNjgyMTc@._V1_UX182_CR0,0,182,268_AL_.jpg",
        		"https://www.youtube.com/embed/Z5ezsReZcxU?modestbranding=1",
          "A tale of a dystopian future where an unknown alien group have colonised the earth and humans struggle to fight back.");
      }



    
    private Director directorFromString(String string){
    	return new Director(string);
    }
    
    private Set<Director> directors(List<Director> dir){
    	return new HashSet<Director>(dir);
    }
}