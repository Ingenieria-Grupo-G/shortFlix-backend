package com.boot.ember.model;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;

public class WriterTest {
	
	private ShortFilm mockShort;
	private Writer writer;
	
	@Before
	public void setUp(){
		mockShort = mock(ShortFilm.class);
		writer = new Writer("Josh Cooley");
	}
	

	@Test
	public void testWriterCreation() {
		assertEquals("Josh Cooley", writer.getName() );
		assertTrue(writer.getWrittenShorts().isEmpty());
	}
	
	@Test
	public void testActorStarredAShort(){
		writer.addWrittenShorts(mockShort);
		assertFalse(writer.getWrittenShorts().isEmpty());
	}

}