package com.boot.ember.model;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;

public class ActorTest {
	
	private ShortFilm mockShort;
	private Actor actor;
	
	@Before
	public void setUp(){
		mockShort = mock(ShortFilm.class);
		actor = new Actor("Jason Topolski");
	}
	

	@Test
	public void testActorCreation() {
		assertEquals("Jason Topolski", actor.getName() );
		assertTrue(actor.getStarredShorts().isEmpty());
	}
	
	@Test
	public void testActorStarredAShort(){
		actor.addStarredShort(mockShort);
		assertFalse(actor.getStarredShorts().isEmpty());
	}

}