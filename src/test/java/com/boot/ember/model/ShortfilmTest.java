package com.boot.ember.model;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashSet;

import org.junit.Test;

public class ShortfilmTest {

	@Test
	public void testShortfilmCreation() {
		Director director = new Director("Josh Cooley");
		Writer writer = new Writer("Josh Cooley");
		ShortFilm shortfilm = new ShortFilm("George and AJ", "", 
				"Exclusive Pixar short featuring George and AJ, character from the feature film Up",
				0.0, "", 2009, new HashSet<Genre>(Arrays.asList(Genre.COMEDY)),"4:04", 
				new HashSet<Writer>(Arrays.asList(writer)), 
				new HashSet<Actor>(Arrays.asList(new Actor("Jason Topolski"), new Actor("A.J. Rienli III"))), 
				new HashSet<Director>(Arrays.asList(director)), 
				"https://www.youtube.com/watch?v=hF5KWMX3u4Y");
		assertEquals("George and AJ", shortfilm.getTitle());
	}

}
