package com.boot.ember.services;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import com.boot.ember.dao.UserDAO;
import com.boot.ember.model.Actor;
import com.boot.ember.model.Director;
import com.boot.ember.model.Genre;
import com.boot.ember.model.ShortFilm;
import com.boot.ember.model.User;
import com.boot.ember.model.Writer;


public class ShortflixServiceTest {
	
	private ShortflixService shortflixService;

	@Test
	public void test_creation_of_short_in_db() {
		
		this.shortflixService = new ShortflixService();
		Director director = new Director("Josh Cooley");
		Writer writer = new Writer("Josh Cooley");
		
		// short 1: George and AJ
		this.shortflixService.saveShortfilm(new ShortFilm("George and AJ", "", 
		"Exclusive Pixar short featuring George and AJ, character from the feature film Up",
		0.0, "", 2009, new HashSet<Genre>(Arrays.asList(Genre.ANIMATION, Genre.COMEDY)),"4:04", 
		new HashSet<Writer>(Arrays.asList(writer)), 
		new HashSet<Actor>(Arrays.asList(new Actor("Jason Topolski"), new Actor("A.J. Rienli III"))),
		new HashSet<Director>(Arrays.asList(director)), 
		"https://www.youtube.com/watch?v=hF5KWMX3u4Y"));
		ShortFilm georgeAndAJSF = this.shortflixService.getShortfilm(1L);
		Set<Genre> expectedGenres = new HashSet<Genre>(Arrays.asList(Genre.ANIMATION, Genre.COMEDY));
		assertEquals("George and AJ", georgeAndAJSF.getTitle());
		assertEquals(expectedGenres, georgeAndAJSF.getGenres());
		
	}
	
	@Test
	public void test_creation_of_user_in_db(){
		this.shortflixService = new ShortflixService();
		User user = new User("quentin", "quentin@tarantino.com", "mrbrown123");
		this.shortflixService.saveUser(user);
		User retrievedUser = this.shortflixService.getUser("quentin@tarantino.com");
		assertEquals(user.getEmail(), retrievedUser.getEmail());
	}

}
